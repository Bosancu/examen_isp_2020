package Marius;
import javax.swing.*;
import java.awt.event.*;

public class Main {
    Main() {
        JFrame frame = new JFrame("s2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setResizable(false);
        frame.setLayout(null);

        JTextField t1 = new JTextField(); // name of the text fild

        t1.setBounds(10, 10, 200, 30);

        frame.add(t1);

        JButton b1 = new JButton("inversare");

        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                char[] text = t1.getText().toCharArray();

                for (int i = text.length-1; i>=0; i--)
                    System.out.print(text[i]);
            }
        });

        b1.setBounds(10, 200, 80, 20);

        frame.add(b1);

        frame.setVisible(true);
    }


    public static void main(String[] args) {
        new Main();
    }
}